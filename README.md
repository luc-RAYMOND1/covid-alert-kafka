# covid-alert-kafka-docker

Kafka project used to launch kafka with docker for the Covid Alert project (IWA)

## Installation

`> git clone https://gitlab.com/luc-RAYMOND1/covid-alert-kafka.git`

## Usage

In the project directory, run:

`> docker-compose -f docker-compose.yml up -d`
